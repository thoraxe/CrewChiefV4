﻿namespace CrewChiefV4.UserInterface
{
    partial class MyName_V
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyName_V));
            this.labelEnterYourName = new System.Windows.Forms.Label();
            this.textBoxMyName = new System.Windows.Forms.TextBox();
            this.listBoxPersonalisations = new System.Windows.Forms.ListBox();
            this.listBoxDriverNames = new System.Windows.Forms.ListBox();
            this.labelFullPersonalisation = new System.Windows.Forms.Label();
            this.labelDriverName = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonPlayName = new System.Windows.Forms.Button();
            this.buttonNameSelect = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelEnterYourName
            // 
            this.labelEnterYourName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelEnterYourName.AutoSize = true;
            this.labelEnterYourName.Location = new System.Drawing.Point(121, 0);
            this.labelEnterYourName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelEnterYourName.Name = "labelEnterYourName";
            this.labelEnterYourName.Size = new System.Drawing.Size(84, 22);
            this.labelEnterYourName.TabIndex = 0;
            this.labelEnterYourName.Text = "Enter your name";
            this.labelEnterYourName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxMyName
            // 
            this.textBoxMyName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMyName.Location = new System.Drawing.Point(209, 2);
            this.textBoxMyName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxMyName.MaxLength = 32;
            this.textBoxMyName.Name = "textBoxMyName";
            this.textBoxMyName.Size = new System.Drawing.Size(203, 20);
            this.textBoxMyName.TabIndex = 1;
            this.textBoxMyName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxMyName_KeyDown);
            // 
            // listBoxPersonalisations
            // 
            this.listBoxPersonalisations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxPersonalisations.Enabled = false;
            this.listBoxPersonalisations.FormattingEnabled = true;
            this.listBoxPersonalisations.Location = new System.Drawing.Point(10, 43);
            this.listBoxPersonalisations.Margin = new System.Windows.Forms.Padding(10, 2, 10, 2);
            this.listBoxPersonalisations.Name = "listBoxPersonalisations";
            this.listBoxPersonalisations.Size = new System.Drawing.Size(187, 134);
            this.listBoxPersonalisations.TabIndex = 1;
            this.listBoxPersonalisations.SelectedIndexChanged += new System.EventHandler(this.listBoxPersonalisations_SelectedIndexChanged);
            this.listBoxPersonalisations.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxPersonalisations_MouseDoubleClick);
            // 
            // listBoxDriverNames
            // 
            this.listBoxDriverNames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxDriverNames.Enabled = false;
            this.listBoxDriverNames.FormattingEnabled = true;
            this.listBoxDriverNames.Location = new System.Drawing.Point(217, 43);
            this.listBoxDriverNames.Margin = new System.Windows.Forms.Padding(10, 2, 10, 2);
            this.listBoxDriverNames.Name = "listBoxDriverNames";
            this.listBoxDriverNames.Size = new System.Drawing.Size(187, 134);
            this.listBoxDriverNames.TabIndex = 2;
            this.listBoxDriverNames.SelectedIndexChanged += new System.EventHandler(this.listBoxDriverNames_SelectedIndexChanged);
            this.listBoxDriverNames.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxDriverNames_MouseDoubleClick);
            // 
            // labelFullPersonalisation
            // 
            this.labelFullPersonalisation.AutoSize = true;
            this.labelFullPersonalisation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelFullPersonalisation.Location = new System.Drawing.Point(2, 28);
            this.labelFullPersonalisation.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelFullPersonalisation.Name = "labelFullPersonalisation";
            this.labelFullPersonalisation.Size = new System.Drawing.Size(203, 13);
            this.labelFullPersonalisation.TabIndex = 3;
            this.labelFullPersonalisation.Text = "Full personalisation";
            this.labelFullPersonalisation.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labelDriverName
            // 
            this.labelDriverName.AutoSize = true;
            this.labelDriverName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelDriverName.Location = new System.Drawing.Point(209, 28);
            this.labelDriverName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelDriverName.Name = "labelDriverName";
            this.labelDriverName.Size = new System.Drawing.Size(203, 13);
            this.labelDriverName.TabIndex = 4;
            this.labelDriverName.Text = "Driver name";
            this.labelDriverName.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.listBoxDriverNames, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelDriverName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.listBoxPersonalisations, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxMyName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelEnterYourName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelFullPersonalisation, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonPlayName, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.buttonNameSelect, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.77778F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.22222F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 148F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(414, 214);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // buttonPlayName
            // 
            this.buttonPlayName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPlayName.Enabled = false;
            this.buttonPlayName.Image = ((System.Drawing.Image)(resources.GetObject("buttonPlayName.Image")));
            this.buttonPlayName.Location = new System.Drawing.Point(10, 191);
            this.buttonPlayName.Margin = new System.Windows.Forms.Padding(10, 2, 10, 2);
            this.buttonPlayName.Name = "buttonPlayName";
            this.buttonPlayName.Size = new System.Drawing.Size(187, 21);
            this.buttonPlayName.TabIndex = 5;
            this.buttonPlayName.UseVisualStyleBackColor = true;
            this.buttonPlayName.Click += new System.EventHandler(this.buttonPlayName_Click);
            // 
            // buttonNameSelect
            // 
            this.buttonNameSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNameSelect.Enabled = false;
            this.buttonNameSelect.Location = new System.Drawing.Point(217, 191);
            this.buttonNameSelect.Margin = new System.Windows.Forms.Padding(10, 2, 10, 2);
            this.buttonNameSelect.Name = "buttonNameSelect";
            this.buttonNameSelect.Size = new System.Drawing.Size(187, 21);
            this.buttonNameSelect.TabIndex = 6;
            this.buttonNameSelect.Text = "Select";
            this.buttonNameSelect.UseVisualStyleBackColor = true;
            this.buttonNameSelect.Click += new System.EventHandler(this.buttonNameSelect_Click);
            // 
            // MyName_V
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 255);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "MyName_V";
            this.Text = "Selecting a name";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelEnterYourName;
        private System.Windows.Forms.TextBox textBoxMyName;
        private System.Windows.Forms.ListBox listBoxPersonalisations;
        private System.Windows.Forms.ListBox listBoxDriverNames;
        private System.Windows.Forms.Label labelFullPersonalisation;
        private System.Windows.Forms.Label labelDriverName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonPlayName;
        private System.Windows.Forms.Button buttonNameSelect;
    }
}